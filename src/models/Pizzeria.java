package models;



public class Pizzeria {
    Factory factory;

    public Pizzeria(){
        factory = new Factory();
    }

    public Pizza commanderPizza(String nom){

        Pizza pizza = factory.createPizza(nom);

        pizza.preparer();
        pizza.cuire();
        pizza.couper();
        pizza.emballer();

        return  pizza;


    }
}
