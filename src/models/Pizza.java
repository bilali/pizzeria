package models;

import java.util.ArrayList;

public abstract class Pizza {
    protected  String nom, sauce, pate;
    protected  ArrayList ingrediens = new ArrayList();


    public void preparer(){
        System.out.printf("Preparation de la pizza" + nom);
        System.out.println("Preparation de la pate");
        System.out.println("Rajouter la sauce ...");
        System.out.println("Rajouter les ingredients:");

        for (int i =0; i<ingrediens.size(); i++ ) {
            System.out.println(" " + ingrediens.get(i));
        }
    }

    public void cuire(){
        System.out.println("Cuisson");
    }

    public void couper(){
        System.out.println("Decoupe");
    }

    public void emballer(){
        System.out.println("Emballage");
    }
}
