package models;

public interface FactoryIf {

    public Pizza  createPizza(String nom);

    public  Article createArticle();
}
