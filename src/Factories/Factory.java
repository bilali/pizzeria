package models;

public class Factory implements  FactoryIf {
    @Override
    public Pizza createPizza(String nom) {
        Pizza pizza;

        switch (nom){
            case "Pizza4Fromaget":
                pizza = new Pizza4Fromaget();
                break;
            case "PizzaPoulet":
                pizza = new PizzaPoulet();
                break;
            case "PizzaVegetarienne":
                pizza = new PizzaVegetarienne();
                break;
            default:
                pizza = new PizzaVH();

        }

        return pizza;
    }
}
